EMR Basics - Registration
=========================

This is a simple convenience module for adding a Registration ( Intake Form ) to
a Practice website. The module implements a form for collecting patient
demographics and a views defination for creating reports. Additionally, there is
a default view to list patient information.

Intallation
-----------

The module is currently a sandbox project.

Dependencies:
-------------
The module utilizes the following modules:

  date_api
  date_popup
  fapi_validation
  masked_input
  print
  views

Additionally it uses the class:

  fngssn.php



