<?php

/**
 * @file
 * Default View for the patients page.
 */

/**
 * Implements hook_views_default_views().
 */
function registration_views_default_views() {
$view = new view();
  $view->name = 'patients';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'emrbasics_registration';
  $view->human_name = 'Patients';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Patient List - Brief';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
    4 => '4',
    5 => '5',
  );
    $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'fname_1' => 'fname_1',
    'lname' => 'lname',
    'mname' => 'mname',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'fname_1' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'lname' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'mname' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );

  /* Field: Patients: Patient ID */
  $handler->display->display_options['fields']['p_id']['id'] = 'p_id';
  $handler->display->display_options['fields']['p_id']['table'] = 'emrbasics_registration';
  $handler->display->display_options['fields']['p_id']['field'] = 'p_id';
  /* Field: Patients: Last Name */
  $handler->display->display_options['fields']['lname']['id'] = 'lname';
  $handler->display->display_options['fields']['lname']['table'] = 'emrbasics_registration';
  $handler->display->display_options['fields']['lname']['field'] = 'lname';
  /* Field: Patients: First Name */
  $handler->display->display_options['fields']['fname_1']['id'] = 'fname_1';
  $handler->display->display_options['fields']['fname_1']['table'] = 'emrbasics_registration';
  $handler->display->display_options['fields']['fname_1']['field'] = 'fname';
  /* Field: Patients: Middle Name */
  $handler->display->display_options['fields']['mname']['id'] = 'mname';
  $handler->display->display_options['fields']['mname']['table'] = 'emrbasics_registration';
  $handler->display->display_options['fields']['mname']['field'] = 'mname';
  /* Field: Patients: City */
  $handler->display->display_options['fields']['city']['id'] = 'city';
  $handler->display->display_options['fields']['city']['table'] = 'emrbasics_registration';
  $handler->display->display_options['fields']['city']['field'] = 'city';
  /* Field: Patients: State */
  $handler->display->display_options['fields']['state']['id'] = 'state';
  $handler->display->display_options['fields']['state']['table'] = 'emrbasics_registration';
  $handler->display->display_options['fields']['state']['field'] = 'state';
  /* Field: Patients: Zip */
  $handler->display->display_options['fields']['zip']['id'] = 'zip';
  $handler->display->display_options['fields']['zip']['table'] = 'emrbasics_registration';
  $handler->display->display_options['fields']['zip']['field'] = 'zip';
  /* Sort criterion: Patients: Last Name */
  $handler->display->display_options['sorts']['lname']['id'] = 'lname';
  $handler->display->display_options['sorts']['lname']['table'] = 'emrbasics_registration';
  $handler->display->display_options['sorts']['lname']['field'] = 'lname';
  $handler->display->display_options['sorts']['lname']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['lname']['expose']['label'] = 'Last Name';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'patients';

  $views[$view->name] = $view;
  return $views;
}

