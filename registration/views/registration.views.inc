<?php

/**
 * @file
 * Views hooks for Patient Registrations.
 */

/**
 * Implements hook_views_data().
 */
function registration_views_data() {

  // Basic table information.
  $data['emrbasics_registration']['table']['group']  = t('Patients');

  // Advertise this table as a possible base table.
  $data['emrbasics_registration']['table']['base'] = array(
   'field' => 'p_id',
   'title' => t('Patient Information'),
   'help' => t('Patient Registration Information.'),
   'weight' => 10,
  );

  $data['emrbasics_registration']['p_id'] = array(
    'title' => t('Patient ID'),
    'help' => t('The Patient ID for EMR Basics.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
      'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

$data['emrbasics_registration']['fname'] = array(
    'title' => t('First Name'),
    'help' => t('The First Name or given name of the patient being Registered.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ), 

    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
$data['emrbasics_registration']['mname'] = array(
    'title' => t('Middle Name'),
    'help' => t('The Middle Name of the patient being Registered.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => FALSE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

$data['emrbasics_registration']['lname'] = array(
    'title' => t('Last Name'),
    'help' => t('The Last Name or Surname of the Patient being Registered.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
        'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
$data['emrbasics_registration']['city'] = array(
    'title' => t('City'),
    'help' => t('The City of the Patient being Registered.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
        'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

$data['emrbasics_registration']['state'] = array(
    'title' => t('State'),
    'help' => t('The State of the Patient being Registered.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
        'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

$data['emrbasics_registration']['zip'] = array(
    'title' => t('Zip'),
    'help' => t('The Zip Code of the Patient being Registered.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
        'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  
$data['emrbasics_registration']['zip'] = array(
    'title' => t('Zip'),
    'help' => t('The Zip Code of the Patient being Registered.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
        'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

$data['emrbasics_registration']['gender'] = array(
    'title' => t('Gender'),
    'help' => t('The Gender of the Patient being Registered.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
        'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

$data['emrbasics_registration']['maritalstatus'] = array(
    'title' => t('Marital Status'),
    'help' => t('The Marital Status of the Patient being Registered.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
        'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

$data['emrbasics_registration']['driverslicense'] = array(
    'title' => t('Drivers License'),
    'help' => t('The Drivers License of the Patient being Registered.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
        'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
 $data['emrbasics_registration']['socialsecurity'] = array(
    'title' => t('Social Security'),
    'help' => t('The Social Security of the Patient being Registered.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
        'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

 $data['emrbasics_registration']['phone'] = array(
    'title' => t('Phone'),
    'help' => t('The Primary Phone of the Patient being Registered.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
        'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
   $data['emrbasics_registration']['wphone'] = array(
    'title' => t('Work Phone'),
    'help' => t('The Work place Phone of the Patient being Registered.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
        'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

   $data['emrbasics_registration']['hphone'] = array(
    'title' => t('Home Phone'),
    'help' => t('The Home Phone of the Patient being Registered.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
        'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

return $data;
}
